demoApp.filter('gender', function() {
  return function(input) {
    switch(input){
    	case 0:
    		gender = '男';
    		break;
    	case 1:
    		gender = '女';
    		break;
    	default:
    		gender = '不详';
    }
    return gender;
  };
});