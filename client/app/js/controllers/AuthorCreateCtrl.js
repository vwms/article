demoApp.controller('AuthorCreateCtrl', function($scope, $location, Author){
	$scope.submit = function(isValid, author){
		$scope.submitted = true;
		$scope.authorCreateForm.$setDirty();

		if (!isValid) {
			return;
		}

		Author.create(author).then(function(response) {
			console.log($location.path());
			$location.url('/authors');
		});
	};
})
