demoApp.controller('QuoteCreateCtrl', function($scope, $location, Quote, Author){
	Author.get().then(function(response){
		$scope.authors = response.authors;
	});
	$scope.submit = function(isValid, quote){
		$scope.submitted = true;
		$scope.quoteCreateForm.$setDirty();

		if (!isValid) {
			return;
		}
		
		quote.author_id = $scope.author.id;

		Quote.create(quote).then(function(response) {
			$location.path('./#/quotes');
		});
	};
})

