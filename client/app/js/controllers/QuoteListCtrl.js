demoApp.controller('QuoteListCtrl', function($scope, $location, $filter, Quote) {
	

	// excuted when page is loaded;
	Quote.get().then(function(response) {
		$scope.quotes = response.quotes;
		$scope.message = response.message;
		console.log(response, $scope.quotes,$scope.message)
	});


	// excuted when query-by-date-range-btn is clicked;
	$scope.query_by_date = function() {
		start_date = $filter('date')($scope.start_date, "yyyy-MM-dd");
		end_date = $filter('date')($scope.end_date, "yyyy-MM-dd");
		// console.log(start,end);
		Quote.getByDateRange(start_date, end_date).then(function(response){
			$scope.quotes = response.quotes;
			console.log(response, $scope.quotes);
		});
	};


	// a array of selected-quote's id;
	$scope.selectedQuotesId = [];


	// excuted when select input is clicked;
	$scope.check = function(quoteId){
		if ($scope.selectedQuotesId.indexOf(quoteId) === -1) {
			// add to selectedQuotesId;
			$scope.selectedQuotesId.push(quoteId);
		}
		else {
			// remove form selectedQuotesId;
			$scope.selectedQuotesId.splice($scope.selectedQuotesId.indexOf(quoteId), 1);
		}
		console.log($scope.selectedQuotesId);
	};


	//excuted when delete-btn is clicked
	$scope.remove = function(){
		//为了与接口一致，需要将其转化为{"quote_id": [1, 2, ...]}
		var quote = {};
		quote.quote_id = $scope.selectedQuotesId;
		console.log(quote);

		Quote.remove(quote).then(function(response){
			console.log(response);
			$location.path('./#/quotes');
		});
	};
})