demoApp.factory('Author', function(Restangular) {
    
    var Author;
    
    Author = {
        get: function() {
            return Restangular.one('authors').get();
        },
        getById: function(id) {
            return Restangular.one('authors', id).get();
        },
        create: function(data) {
            return Restangular
                .one('authors')
                .customPOST(data);
        }
    };
    
    return Author;  
})