demoApp.factory('Quote', function(Restangular) {
	
	var Quote;

	Quote = {
		get: function() {
			// get all quotes;
			return Restangular.one('quotes').get();
		},
		getById: function(id) {
			// get a quote by id;
			return Restangular.one('quotes').get({id: id});
		},
		getByDate: function(date) {
			// get quotes by date;
			return Restangular.one('quotes').get({posted_at: start});
		},
		getByDateRange: function(start_date, end_date) {
			// get quotes between startDate and endDate;
			return Restangular.one('quotes').get({startDate: start_date, endDate: end_date});
		},
		// customGet: function(...needles) {
		// 	// need to use array as params;
		// 	// for example: [];
		// 	// return 0;
		// },
		create: function(data){
			// console.log(data);
			return Restangular.one('quotes').customPOST(data);
		},
		remove: function(data){
			// data's style is depend on the API requirement;
			// style for now: object{"author_id": [1, 2, ...]}, should be handled by controller;
			// console.log(data);
			return Restangular.one('quotes').customOperation('remove', null, null, null, data);
		}
	};

	return Quote;
})