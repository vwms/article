window.demoApp = angular.module('demoApp', ['ngRoute', 'restangular']);

demoApp.config(['$routeProvider','RestangularProvider', function($routeProvider, RestangularProvider) {    
    // RestangularProvider.setBaseUrl('http://192.168.5.105:5000/');
    // RestangularProvider.setBaseUrl('http://192.168.5.104:5000/');
    RestangularProvider.setBaseUrl('http://localhost:5000/');
    RestangularProvider.setDefaultHeaders({'Content-Type': 'application/json'});

    RestangularProvider.addFullRequestInterceptor(function(headers, params, element, httpConfig){
        console.log(headers, params, element, httpConfig);
    })

	$routeProvider
        .when('/', {
            redirectTo: '/quotes'
        })
    	.when('/authors', {
    		controller: 'AuthorListCtrl',
            templateUrl: '/templates/author/list.html'
    	})
        .when('/authors/create', {
            controller: 'AuthorCreateCtrl',
            templateUrl: '/templates/author/create.html'
        })
        .when('/quotes', {
            controller: 'QuoteListCtrl',
            templateUrl: '/templates/quote/list.html'
        })
        .when('/quotes/create', {
            controller: 'QuoteCreateCtrl',
            templateUrl: '/templates/quote/create.html'
        })
        .otherwise({
            redirectTo: '/quotes'
        })
}]);

