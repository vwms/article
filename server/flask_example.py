#coding:utf-8
'''
Created on 2016��4��6��
@author: shuhewei
'''

import datetime
from flask import Flask, jsonify, request
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy.exc import IntegrityError
from marshmallow import Schema, fields, ValidationError, pre_load
from flask.globals import session
from sqlalchemy.sql.expression import and_


app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite:///quotes.db'
db = SQLAlchemy(app, use_native_unicode="utf8")

##### MODELS #####

class Author(db.Model):
    '''添加Author类'''
    id = db.Column(db.Integer, primary_key=True)
    first = db.Column(db.String(80))
    last = db.Column(db.String(80))
    gender = db.Column(db.Integer())


class Quote(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String, nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey("author.id"))
    author = db.relationship("Author",
                        backref=db.backref("quotes", lazy="dynamic"))
    posted_at = db.Column(db.DateTime) 

##### SCHEMAS #####
class AuthorSchema(Schema):
    id = fields.Int(dump_only=True)
    first = fields.Str()
    last = fields.Str()
    gender = fields.Int()
    formatted_name = fields.Method("format_name", dump_only=True)

    def format_name(self, author):
        return "{}, {}".format(author.last.encode('utf8'), author.first.encode('utf8'))

# Custom validator
def must_not_be_blank(data):
    if not data:
        raise ValidationError('Data not provided.')
    

class CreatedorQueryQuoteSchema(Schema):
    '''创建Quotes字段中需要的字段'''
    id = fields.Int(dump_only=True)
    author = fields.Nested(AuthorSchema, validate=must_not_be_blank)
    content = fields.Str(required=True, validate=must_not_be_blank)
    posted_at = fields.DateTime(dump_only=True)
    
    
# 需要重新修改
class QuoteSchema(Schema):
    id = fields.Int(dump_only=True)
    content = fields.Str(required=True, validate=must_not_be_blank)
    posted_at = fields.DateTime(dump_only=True)

    # Allow client to pass author's full name in request body
    # e.g. {"author': 'Tim Peters"} rather than {"first": "Tim", "last": "Peters"}
    @pre_load
    def process_author(self, data):
        return data

# 需要重新修改
class QuoteSchemaQuery(Schema):
    id = fields.Int(dump_only=True)
    author = fields.Nested(AuthorSchema, validate=must_not_be_blank)
    content = fields.Str(required=True, validate=must_not_be_blank)
    posted_at = fields.DateTime(dump_only=True)

    # Allow client to pass author's full name in request body
    # e.g. {"author': 'Tim Peters"} rather than {"first": "Tim", "last": "Peters"}
    @pre_load
    def process_author(self, data):
        author_name = data.get('author')
        if author_name:
            first, last = author_name.split(' ')
            gender = data.get('gender')
            author_dict = dict(first=first, last=last,gender=gender)
        else:
            author_dict = {}
        data['author'] = author_dict
        return data
    
author_schema = AuthorSchema()
authors_schema = AuthorSchema(many=True)
quote_schema = QuoteSchema()
quote_schema_query = QuoteSchemaQuery(many=True)
quote_schema_create = CreatedorQueryQuoteSchema()
quotes_schema = CreatedorQueryQuoteSchema(many=True, only=('id', 'content'))

##### API #####

@app.route('/authors', methods=['GET'])
def get_authors():
        authors = Author.query.all()
        # Serialize the queryset
        result = authors_schema.dump(authors)
        return jsonify({"message":"Query author OK",'authors': result.data})


@app.route("/authors/<int:pk>")
def get_author(pk):
    try:
        author = Author.query.get(pk)
    except IntegrityError:
        return jsonify({"message": "Author could not be found."}), 400
    author_result = author_schema.dump(author)
    quotes_result = quotes_schema.dump(author.quotes.all())
    return jsonify({'authors': author_result.data, 'quotes': quotes_result.data})

@app.route("/quotes/<int:pk>")
def get_quote(pk):
    try:
        quote = Quote.query.get(pk)
    except IntegrityError:
        return jsonify({"message": "Quote could not be found."}), 400
    result = quote_schema.dump(quote)
    return jsonify({"quotes": result.data})


@app.route("/quotes", methods=['GET'])
def get_quote_by_date():
    '''使用模糊查詢
    '''
    #:使用范围查询
    
    if len(request.args) == 2:
        try:
            quote = get_quote_date(request)
        except:
            return jsonify({"message":"quote could not be found."}),400
        result = quote_schema_query.dump(quote)
    #：使用日期模糊查询
    elif len(request.args) == 1:
        try:
            date_value = request.args.get('posted_at')
            quote = Quote.query.filter(Quote.posted_at.like('%'+date_value+'%')).all()
        except Exception:
            return jsonify({"message":"quote could not be found."}),400
        result = quote_schema_query.dump(quote)
    #: 使用日期分頁查詢
    elif len(request.args) == 4:
        try:
            quote = get_quote_by_paging(request)
        
        except Exception:
            return jsonify({"message":"quote could not be found."}),400
        
        result = quote_schema_query.dump(quote)
            
                
    else:
        quotes = Quote.query.all()
        result = quote_schema_query.dump(quotes)
    
    return jsonify({"message":"Query quote OK",
                    "quotes": result.data})
    

def get_quote_date(request):
    '''时间范围查询'''
    date_begin = request.args.get('startDate')
    date_end = request.args.get('endDate')
    try:
        date_1 = datetime.datetime.strptime(date_end, "%Y-%m-%d")
        end_date = date_1 + datetime.timedelta(days=1)
        quote = Quote.query.filter(and_(Quote.posted_at >= date_begin,Quote.posted_at <= end_date))
    except Exception,e:
        print e
    return quote

@app.route("/quotes", methods=["POST"])
def new_quote():
    '''添加quote的对象'''
    
    json_data = request.get_json()
    if not json_data:
        return jsonify({'message': 'No input data provided'}), 400
    # Validate and deserialize input
    data, errors = quote_schema.load(json_data)
    if errors:
        return jsonify(errors), 422
    
    author = Author.query.filter_by(id=json_data['author_id']).first()
    if author is None:
        return jsonify({"message": "the quote has no author"})
    
    # Create new quote
    quote = Quote(
        content=data['content'],
        author_id=json_data['author_id'],
        posted_at=datetime.datetime.utcnow()
    )

    db.session.add(quote)
    db.session.commit()
    result = quote_schema_create.dump(Quote.query.get(quote.id))
    return jsonify({"message": "Create quote OK",
                    "quotes": result.data})
    
    
@app.route("/authors", methods=["POST"])
def new_author():
    '''添加author对象'''
    json_data = request.get_json()
    if not json_data:
        return jsonify({'message':'No input data provided'}), 400
    data, errors = author_schema.load(json_data)
    if errors:
        return jsonify(errors),422
    first, last, gender = data['first'], data['last'], data['gender']
    author = Author.query.filter_by(first=first, last=last).first()
    if author is None:
        #Create a new author
        author = Author(first=first, last=last, gender=gender)
        db.session.add(author)
    
    db.session.commit()
    result = author_schema.dump(Author.query.get(author.id))
    return jsonify({"message": "Create author OK", "author": result.data})

@app.route("/quotes",methods=["DELETE"])
def del_quote():
    '''添加删除接口'''
    
    data = request.get_json()
    id_list = data['quote_id']
    for item_id in id_list:
        db.session.delete(Quote.query.filter_by(id = item_id).first())   
    db.session.commit()
    return "Delete quote OK"

def get_quote_by_paging(request):
    '''分页查询'''
    
    try:
        #: 对请求参数进行处理
        start_date = request.args.get('startDate')
        end_date = request.args.get('endDate')
        start_page = request.args.get('start')
        per_page = request.args.get('pageSize')
        
        #: 第一种分页的方法
        order_quote = Quote.query.filter(and_(Quote.posted_at >= start_date,Quote.posted_at <= end_date)).order_by('%s desc'%Quote.posted_at)
        pagination = order_quote.paginate(int(start_page),int(per_page),False)
        quotes = pagination.items

        #：第二种分页的方法
        '''
        paging = Quote.query.order_by('%s desc'%Quote.posted_at).limit(3).all()
        
        print quote_schema_query.dump(paging)
        '''
    
    except Exception,e:
        print e
    
    return quotes
#     Quote.query.paginate(start_page,per_page,False)
#     some_object = session.query(VersionedFoo).get((5, 10))

    
    
if __name__ == '__main__':
    db.create_all()
    app.run(debug=True,port=5000)
    
    
    
    
    
