# -*- coding: utf-8 -*
import unittest
import json
import os,sys  
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  
sys.path.insert(0,parentdir)  
from flask_example import app
from bottle import response

class ModAuthTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app
        #: 启动一个测试客户端 
        self.test_app = self.app.test_client()

    def tearDown(self):
        #: 销毁对象空间
        self.test_app=None

    def addAuthor(self, first, last, gender):
        '''添加Author字段的参数值
        '''
        
        data = dict(first=first, last=last, gender=gender)
        json_data = json.dumps(data)
        return self.test_app.post('/authors', 
            content_type='application/json;charset=utf-8',
            data = json_data)
        
    @unittest.skip("has test")
    def get_quotes(self):
        quotes_result = self.test_app.get('/quotes?startDate=2016-04-10&endDate=2016-04-12', 
            content_type='application/json;charset=utf-8')
        
        
        return quotes_result
    @unittest.skip("this author data has been in the db")    
    def test_addAuthor(self):
        '''测试添加Author'''
        
        print "---------start test author---------------"
        
        sub_author_resp = {"message":"Create author OK"}
        rv = self.addAuthor("tailai", "smith",0)
        print "这是的到author的数据",rv.get_data()
        #: 测试POST添加Author过程
        self.assertDictContainsSubset(sub_author_resp, json.loads(rv.get_data()), "添加Author失败")
        
        print "-----end test author-------"
        
    @unittest.skip("has test")
    def test_queryAuthor(self):
        '''测试查询Author'''
        
        print '----------------查找author开始----------------------------'
        query_author_rsp = self.test_app.get('/authors',content_type='application/json;charset=utf-8')
        print '-----查询的author字段是多少----',query_author_rsp.get_data()
        author_sub_message = {"message": "Query author OK"}
        self.assertDictContainsSubset(author_sub_message, json.loads(query_author_rsp.get_data()), "查询失败")
        print query_author_rsp.get_data()
        
        print '----------------------查找author结束----------------------------'
    
    @unittest.skip("has test")
    def test_addQuery(self):
        '''测试添加quotes数据'''
        
        print '---------------开始添加Query字段--------------------------------'
        
        add_query_json = {"author_id":2,"content":"test demo 2016-04-12"}
        
        assert_sub_message = {"message":"Create quote OK"}#：添加猜测的数据
        
        add_data_rsp = self.test_app.post('/quotes',content_type='application/json;charset=utf-8',
                                          data=json.dumps(add_query_json))
        
        self.assertDictContainsSubset(assert_sub_message, json.loads(add_data_rsp.get_data()), "添加失败")
        
        print '-----------结束添加Query字段----------------------------------'
        
    @unittest.skip("has test")
    def test_get_quotes(self):
        '''测试查找quotes'''
        
        print "-----begin test get_quotes------"
        #: 测试查询Quotes的数据，范围查询和模糊查询
        self.assertEqual(response.status_code, 200,'返回成功查询的状态')
        quotes_result =  self.get_quotes()
        
        #: 测试需要的字典里面的子元素在实际返回的字典里
        sub_dict = {"message":"Query quote OK"}
        self.assertDictContainsSubset(sub_dict, json.loads(quotes_result.get_data()),"得到quotes元素失败")
        print "-----end test get_quotes---------"
        
    @unittest.skip("has test")    
    def test_delete_quotes(self):
        '''测试删除字段'''
        print '----------开始删除测试-------------------'
        #: 测试删除的数据
        delte_data_dic = {'quote_id':[4,5,6]}
        
        #: 添加预期的子字段作为对比
        sub_rsp = "Delete quote OK"
        
        #: 调用测试接口
        del_data_rsp = self.test_app.delete('/quotes',content_type='application/json;charset=utf-8',
                                          data=json.dumps(delte_data_dic))
        
        self.assertIn(sub_rsp, del_data_rsp.get_data(),"测试删除失败")
        
        print '----------结束删除测试-------------------'
        
    def test_paging_quotes(self):
        '''测试分页字段'''
        print '--------------开始测试分页字段---------------'
        
        #：添加预期的返回子字段进行对比
        sub_rsp = {"message":"Query quote OK"}
        
        #：调用测试接口
        
        paging_data_rsp = self.test_app.get('/quotes?startDate=2016-04-12&endDate=2016-04-14&start=1&pageSize=3',content_type='application/json;charset=utf-8')
        self.assertDictContainsSubset(sub_rsp, json.loads(paging_data_rsp.get_data()), "分页查询失败")
        
        
        
if __name__ == '__main__':
    unittest.main()
    
    
    
    
    
    
